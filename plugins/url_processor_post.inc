<?php

/**
 * Class FacetAPIPostDataProcessorPrettyPaths
 *
 * Obtains facet values from POST data. For sites using the standard url processor.
 */
class FacetAPIPostDataProcessor extends FacetapiUrlProcessorStandard {

  /**
   * Implements FacetapiUrlProcessor::fetchParams().
   *
   * Use $_POST as the source for facet data.
   */
  public function fetchParams() {

    // If this is not an ajax call, use the parent method.
    if($_GET['q'] != 'system/ajax') {
      return parent::fetchParams();
    }
    else {

      $params = array();
      // All the facet data was passed as form values in a POST request.
      foreach ($_POST as $key => $value) {
        if(is_numeric($key)) {
          $params[$key] = $value;
        }
      }

      return $params;
    }
  }


  /**
   * Implements FacetapiUrlProcessor::normalizeParams().
   *
   * Strips the "q" and "page" variables from the params array.
   */
  public function normalizeParams(array $params, $filter_key = 'f') {
    // Exclude all elements of the form.

    if($_GET['q'] != 'system/ajax') {
      return parent::fetchParams();
    }
    else {

      $exclude = array(
        'q',
        'page',
        'XDEBUG_SESSION',
        'keys',
        'search_group',
        'group_key',
        'page_id',
        'env_id',
        'submit',
        'form_build_id',
        'form_token',
        'form_id',
        '_triggering_element_name',
        'triggering_element_value',
        'ajax_html_ids',
        'ajax_page_state',
      );
      return drupal_get_query_parameters($params, $exclude);
    }
  }


}

/**
 * Class FacetAPIPostDataProcessorPrettyPaths
 *
 * Obtains facet values from POST data. For sites using the facetapi pretty paths module.
 */
class FacetAPIPostDataProcessorPrettyPaths extends FacetapiUrlProcessorPrettyPaths {

  /**
   * Fetches parameters from the source, usually $_GET.
   *
   * This method is invoked in FacetapiAdapter::__construct().
   *
   * @return array
   *   An associative array containing the params, usually $_GET.
   */
  /**
   * Implements FacetapiUrlProcessor::fetchParams().
   *
   * Use $_POST as the source for facet data.
   */
  public function fetchParams() {

    // If this is not an ajax call, use the parent method.
    if($_GET['q'] != 'system/ajax') {
      return parent::fetchParams();
    }
    else {

      $params = array();
      // All the facet data was passed as form values in a POST request.
      foreach ($_POST as $key => $value) {
        if(is_numeric($key)) {
          $params['f'][$key] = $value;
        }
      }


      $params['q'] = 'system/ajax';

      return $params;
    }
  }


  public function getFacetPath(array $facet, array $values, $active) {
    if($_GET['q'] != 'system/ajax') {
      return parent::getFacetPath($facet, $values, $active);
    }
    // Might just want to return system/ajax, or nothing at all here.
    return $this->adapter->getSearchPath();
  }

  /**
   * Normalizes the array returned by FacetapiAdapter::fetchParams().
   *
   * When extracting data from a source such as $_GET, there are certain items
   * that you might nor want, for example the "q" or "page" keys. This method is
   * useful for filtering those out. In addition, plugins that do not get data
   * from $_GET can use this method to normalize the data into an associative
   * array that closely matches the data structure of $_GET.
   *
   * @param array $params
   *   An array of keyed params, usually as $_GET.
   * @param string $filter_key
   *   The array key in $params containing the facet data, defaults to "f".
   *
   * @return array
   *   An associative array containing the normalized params.
   */
  /**
   * Implements FacetapiUrlProcessor::normalizeParams().
   *
   * Strips the "q" and "page" variables from the params array.
   */
  public function normalizeParams(array $params, $filter_key = 'f') {
    // Exclude all elements of the form.

    if($_GET['q'] != 'system/ajax') {
      return parent::normalizeParams($params, $filter_key);
    }
    else {

      $exclude = array(
        'q',
        'page',
        'XDEBUG_SESSION',
        'keys',
        'search_group',
        'group_key',
        'page_id',
        'env_id',
        'submit',
        'form_build_id',
        'form_token',
        'form_id',
        '_triggering_element_name',
        'triggering_element_value',
        'ajax_html_ids',
        'ajax_page_state',
      );
      // Return an array of query parameters.
      return drupal_get_query_parameters($params, $exclude);
    }
  }

  /**
   * Returns the query string variables for a facet item.
   *
   * The return array must be able to be passed as the "query" key of the
   * options array passed as the second argument to the url() function. See
   * http://api.drupal.org/api/drupal/includes%21common.inc/function/url/7 for
   * more details.
   *
   * @param array $facet
   *   The facet definition as returned by facetapi_facet_load().
   * @param array $values
   *   An array containing the item's values being added to or removed from the
   *   query string dependent on whether or not the item is active.
   * @param int $active
   *   An integer flagging whether the item is active or not. 1 if the item is
   *   active, 0 if it is not.
   *
   * @return array
   *   The query string variables.
   */
  public function getQueryString(array $facet, array $values, $active) {
    if($_GET['q'] != 'system/ajax') {
      return parent::getQueryString($facet, $values, $active);
    }
    else {
      // ???
      return 'system/ajax';
    }
  }

  /**
   * Sets the breadcrumb trail for active searches.
   *
   * This method is called by FacetapiAdapter::processFacets(), which is called
   * directly by the backend to search the chain of Facet API events.
   */
  public function setBreadcrumb() {
    if($_GET['q'] != 'system/ajax') {
      return parent::setBreadcrumb();
    }
  }


}