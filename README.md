# Solr Group

## Dependencies

It depends. maybe apachesolr_group. Wondering if we should make this generalizable enough to accomodate
Apachesolr and Search API.

## Installation

Just like any other Drupal module.

## Configuration

Still working that out.

## Goals

1, define a Generalizable back-end to handle solr queries (should be able to use apachesolr or search api).

2, provide an async framework to pull in solr query data.

3, Reformat results in groups.

4, Ajaxy view more links.